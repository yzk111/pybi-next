import _imports
import pytest
from playwright.sync_api import Page, sync_playwright
import pybi as pbi
import pandas as pd
from pathlib import Path
import utils

pbi.meta.set_echarts_renderer("svg")
charts = pbi.easy_echarts


@pytest.fixture(scope="module")
def init_df():
    return pd.DataFrame(
        [
            ["广东省", "广州市", "荔湾区"],
            ["广东省", "广州市", "海珠区"],
            ["广东省", "广州市", "白云区"],
            ["广东省", "深圳市", "南山区"],
            ["广东省", "深圳市", "盐田区"],
            ["广东省", "深圳市", "福田区"],
            ["湖南省", "长沙市", "芙蓉区"],
            ["湖南省", "长沙市", "天心区"],
            ["湖南省", "株洲市", "石峰"],
            ["湖南省", "株洲市", "渌口"],
        ],
        columns=list("省市区"),
    )


@pytest.fixture(scope="module")
def page():

    with sync_playwright() as p:
        browser = p.chromium.launch(headless=True)
        page = browser.new_page()
        yield page


@pytest.fixture(scope="module")
def file_url(init_df):
    file = Path("test_result.html")

    data = pbi.set_source(init_df)

    for name in "省市区":
        pbi.add_slicer(data[name])

    pbi.to_html(file)
    file_url = f"file:///{file.absolute()}"
    yield file_url


@pytest.fixture(scope="function", autouse=True)
def new_page(page: Page, file_url):
    page.goto(file_url)


def test_should_opts_values_省(page: Page, init_df):
    slicer_pvc = utils.PageSlicer(page, "省")
    slicer_pvc.switch_options_pane()

    assert slicer_pvc.get_option_values() == init_df["省"].drop_duplicates().tolist()

    slicer_pvc.switch_options_pane()


def test_should_opts_values_市(page: Page, init_df):
    slicer_city = utils.PageSlicer(page, "市")
    slicer_city.switch_options_pane()

    assert slicer_city.get_option_values() == init_df["市"].drop_duplicates().tolist()

    slicer_city.switch_options_pane()


def test_should_opts_values_区(page: Page, init_df):
    slicer_area = utils.PageSlicer(page, "区")
    slicer_area.switch_options_pane()

    assert slicer_area.get_option_values() == init_df["区"].drop_duplicates().tolist()

    slicer_area.switch_options_pane()


def test_should_opts_values_linkage_省(page: Page, init_df):
    slicer_pvc = utils.PageSlicer(page, "省")
    slicer_pvc.switch_options_pane()

    # 广东省
    slicer_pvc.select_options_by_text("广东省")
    slicer_pvc.switch_options_pane()

    slicer_city = utils.PageSlicer(page, "市")
    slicer_city.switch_options_pane()

    assert slicer_city.get_option_values() == ["广州市", "深圳市"]

    slicer_city.switch_options_pane()

    #
    slicer_area = utils.PageSlicer(page, "区")
    slicer_area.switch_options_pane()

    assert (
        slicer_area.get_option_values()
        == init_df[lambda x: x["省"] == "广东省"]["区"].drop_duplicates().tolist()
    )
    slicer_area.switch_options_pane()
