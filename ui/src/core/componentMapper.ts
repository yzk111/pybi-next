import { ComponentTag } from "@/models/component";
import { type Component } from "vue";
import Box from "@/components/Box.vue";
import FlowBox from "@/components/FlowBox.vue";
import GridBox from "@/components/GridBox.vue";
import ColBox from "@/components/ColBox.vue";
import Slicer from "@/components/Slicer.vue";
import Table from "@/components/Table.vue";
import EChart from "@/components/EChart.vue";
import Upload from "@/components/Upload.vue";
import TextValue from "@/components/TextValue.vue";
import Tabs from "@/components/Tabs.vue";

const mapping = new Map<ComponentTag, Component>(
    [
        [ComponentTag.Box, Box],
        [ComponentTag.ColBox, ColBox],
        [ComponentTag.FlowBox, FlowBox],
        [ComponentTag.GridBox, GridBox],
        [ComponentTag.Upload, Upload],
        [ComponentTag.TextValue, TextValue],
        [ComponentTag.Slicer, Slicer],
        [ComponentTag.Table, Table],
        [ComponentTag.EChart, EChart],
        [ComponentTag.Tabs, Tabs],
    ]
)


export function getComponent(tag: ComponentTag) {
    return mapping.get(tag)!
}