import { ComponentTag } from "@/models/component"
import { TComponentServices } from "@/services/componentServices";


const mBlacklist = new Map<ComponentTag, Set<ComponentTag>>([
    [ComponentTag.Slicer, new Set([ComponentTag.EChart, ComponentTag.Table])]
])


const alwaysFalseFn = (id: string) => {
    return false
}

export function getBlacklistFilterFn(requestorId: string, cpServices: TComponentServices) {
    const getComponent = cpServices.getComponent

    if (requestorId) {

        const requestorCp = getComponent(requestorId)
        const banSet = mBlacklist.get(requestorCp.tag)

        if (banSet) {
            return (id: string) => {
                return banSet.has(getComponent(id).tag)
            }
        }

        return alwaysFalseFn
    }

    return alwaysFalseFn
}