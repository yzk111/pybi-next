import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import * as path from "path";

import AutoImport from 'unplugin-auto-import/vite'
import Components from 'unplugin-vue-components/vite'
import { ElementPlusResolver } from 'unplugin-vue-components/resolvers'
// import { createStyleImportPlugin, VxeTableResolve } from 'vite-plugin-style-import'

import fullImportPlugin from './plugins/fullImportPlugin'



// https://vitejs.dev/config/
export default defineConfig(({ mode }) => {

  return {
    plugins: [
      vue(),
      mode === 'development'
        ? fullImportPlugin()
        : Components({
          resolvers: [ElementPlusResolver()]
        }),

      mode === 'development'
        ? fullImportPlugin()
        : AutoImport({
          resolvers: [ElementPlusResolver()]
        }),

      AutoImport({
        resolvers: [ElementPlusResolver()],
      }),
      Components({
        resolvers: [ElementPlusResolver()],
      }),

      // createStyleImportPlugin({
      //   resolves: [
      //     VxeTableResolve()
      //   ],
      // })
    ],
    resolve: {
      alias: {
        '@': path.resolve(__dirname, 'src')
      }
    },

    test: {
      deps: {
        inline: [
          "lodash",
          'element-plus'
        ]
      },
      environment: 'happy-dom'
    },

    build: {
      // sourcemap: true,
      target: 'esnext',
      assetsInlineLimit: 488280,

      rollupOptions: {
        // external: ['plotly.js-dist'],
        output: {
          // file: '../pyvisflow/template/bundle.js',
          format: 'iife',
          name: 'MyBundle'
        }
      }
    }


  }



})
