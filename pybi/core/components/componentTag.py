from enum import Enum


class ComponentTag(Enum):
    App = "App"
    DataSource = "DataSource"
    Slicer = "Slicer"
    Table = "Table"
    EChart = "EChart"
    Box = "Box"
    Text = "Text"
    Upload = "Upload"
    TextValue = "TextValue"
    ColBox = "ColBox"
    FlowBox = "FlowBox"
    GridBox = "GridBox"
    Tabs = "Tabs"
