import _imports
import pytest
from pybi.core.components import GridBoxComponent


def test_areas_array2str():
    input = [["sc1", "sc2"], ["sc3"], ["table"] * 4]
    act = GridBoxComponent.areas_array2str(input)

    exp = '"sc1 sc2 . ." "sc3 . . ." "table table table table"'

    assert act == exp


def test_areas_str2array():
    input = """
    sc1 sc2
    sc3
    table table table table
"""
    act = GridBoxComponent.areas_str2array(input)

    exp = [["sc1", "sc2"], ["sc3"], ["table", "table", "table", "table"]]

    assert act == exp


def test_areas_str2array_with_space():
    input = """
    sc1  sc2
    sc3
    table  table    table table
"""
    act = GridBoxComponent.areas_str2array(input)

    exp = [["sc1", "sc2"], ["sc3"], ["table", "table", "table", "table"]]

    assert act == exp
