import pathlib
import json
import pandas as pd
import re

m_extract_props_pat = re.compile(r"(\S+:\s*?\S+);")

m_obj_name = "TextAlign"
m_file_name = f"{m_obj_name[0].lower()}{m_obj_name[1:]}"

dest = pathlib.Path(f"pybi/core/styles/tailwindStyles/{m_file_name}.py").absolute()


# if not dest.exists():
#     print(f"目标位置文件不存在[{dest}]")

m_col_class = "Class"
m_col_prop = "Properties"


def gen_each_row():

    df = pd.read_csv("autoScripts/styles.text_color.csv", encoding="utf8")
    df["fn_name"] = df[m_col_class].str.replace(r"-", "_", regex=False)
    # df[["name", "value"]] = df[m_col_prop].str.split(":", expand=True)
    # df["value"] = df["value"].str[:-1]

    for row in df[["fn_name", m_col_prop]].itertuples(index=False):
        data = {}
        for p in m_extract_props_pat.findall(row[1]):
            name, value = p.split(":")
            data[name] = value

        yield row.fn_name, json.dumps(data)


with open(dest, mode="w", encoding="utf8") as f:
    f.write("from pybi.core.styles.styles import StyleBuilder\n")
    f.write(f"class {m_obj_name}:\n")

    for fnName, dict_data in gen_each_row():
        f.write(f"\t@property\n")
        f.write(f"\tdef {fnName}(self):\n")
        f.write(f"\t\treturn StyleBuilder({dict_data})\n")
