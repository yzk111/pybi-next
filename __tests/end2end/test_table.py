import _imports
import pytest
from playwright.sync_api import Page, sync_playwright
import pybi as pbi
import pandas as pd
from pathlib import Path
import utils

pbi.meta.set_echarts_renderer("svg")
charts = pbi.easy_echarts


@pytest.fixture(scope="module")
def init_df():
    df = pd.DataFrame(
        [
            ["广东省", "广州市", "荔湾区"],
            ["广东省", "广州市", "海珠区"],
            ["广东省", "广州市", "白云区"],
            ["广东省", "深圳市", "南山区"],
            ["广东省", "深圳市", "盐田区"],
            ["广东省", "深圳市", "福田区"],
            ["湖南省", "长沙市", "芙蓉区"],
            ["湖南省", "长沙市", "天心区"],
            ["湖南省", "株洲市", "石峰"],
            ["湖南省", "株洲市", "渌口"],
        ],
        columns=list("省市区"),
    )

    df["value"] = range(1, len(df) + 1)
    return df


@pytest.fixture(scope="module")
def page():

    with sync_playwright() as p:
        browser = p.chromium.launch(headless=False)
        page = browser.new_page()
        yield page


@pytest.fixture(scope="module")
def file_url(init_df):
    file = Path("test_result.html")

    data = pbi.set_source(init_df)

    for name in "省市区":
        pbi.add_slicer(data[name])

    pbi.add_table(data).set_debugTag("tab1")

    dv = pbi.set_dataView(f"select 省,sum(value) as total from {data} group by 省")
    pbi.add_table(dv).set_debugTag("tab2")

    pbi.to_html(file)
    file_url = f"file:///{file.absolute()}"
    yield file_url


@pytest.fixture(scope="function", autouse=True)
def new_page(page: Page, file_url):
    page.goto(file_url)


def test_should_slicer_effect_table_rows(page: Page, init_df: pd.DataFrame):

    table1 = utils.PageTable(page, "tab1")
    table2 = utils.PageTable(page, "tab2")

    headers = table1.get_header_cells().all_text_contents()
    assert headers == list(init_df.columns)
    assert table2.get_header_cells().all_text_contents() == ["省", "total"]

    rows = table1.get_rows()
    assert rows.count() == len(init_df)
    assert table2.get_rows().count() == 2

    slicer_pvc = utils.PageSlicer(page, "省")
    slicer_pvc.switch_options_pane()

    slicer_pvc.select_options_by_text("湖南省")
    assert rows.count() == 4
    assert table2.get_rows().count() == 1

    assert table1.get_table_col_values(-1) == list(map(str, range(7, 11)))

    assert table2.get_row_values(0) == ["湖南省", "34"]

    slicer_pvc.clear_selected()
