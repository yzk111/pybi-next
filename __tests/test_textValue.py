import _imports
import pytest
from pybi.core.components import TextValue
from pybi.core.sql import Sql


def test_extract_sql_from_text():
    input = "总销售额:sql:[_ select sum(销售额) from data _]"
    act = list(TextValue.extract_sql_from_text(input))

    exp = ["总销售额:", Sql("select sum(销售额) from data")]

    act_strs = list(a.sql if isinstance(a, Sql) else a for a in act)
    exp_strs = list(e.sql if isinstance(e, Sql) else e for e in exp)

    assert act_strs == exp_strs


def test_extract_sql_from_text_mul_sql():
    input = (
        "总销售额:sql:[_ select sum(销售额) from data _] xxsql:[_ select a,b,c from data _]end"
    )
    act = list(TextValue.extract_sql_from_text(input))

    exp = [
        "总销售额:",
        Sql("select sum(销售额) from data"),
        " xx",
        Sql("select a,b,c from data"),
        "end",
    ]

    act_strs = list(a.sql if isinstance(a, Sql) else a for a in act)
    exp_strs = list(e.sql if isinstance(e, Sql) else e for e in exp)

    assert act_strs == exp_strs


def test_extract_sql_from_text_mul_sql_coiled():
    input = "总销售额:sql:[_ select sum(销售额) from data _]sql:[_ select a,b,c from data _]end xx "
    act = list(TextValue.extract_sql_from_text(input))

    exp = [
        "总销售额:",
        Sql("select sum(销售额) from data"),
        Sql("select a,b,c from data"),
        "end xx ",
    ]

    act_strs = list(a.sql if isinstance(a, Sql) else a for a in act)
    exp_strs = list(e.sql if isinstance(e, Sql) else e for e in exp)

    assert act_strs == exp_strs


def test_extract_sql_from_text_no_sql_flag():
    input = "sql语句[select sum(销售额) from data]的结果是 sql:[_ select sum(销售额) from data _]"
    act = list(TextValue.extract_sql_from_text(input))

    exp = ["sql语句[select sum(销售额) from data]的结果是 ", Sql("select sum(销售额) from data")]

    act_strs = list(a.sql if isinstance(a, Sql) else a for a in act)
    exp_strs = list(e.sql if isinstance(e, Sql) else e for e in exp)

    assert act_strs == exp_strs


def test_extract_sql_from_text_all_sql():
    input = "sql:[_ select sum(销售额) from data _]"
    act = list(TextValue.extract_sql_from_text(input))

    exp = [Sql("select sum(销售额) from data")]

    act_strs = list(a.sql if isinstance(a, Sql) else a for a in act)
    exp_strs = list(e.sql if isinstance(e, Sql) else e for e in exp)

    assert act_strs == exp_strs
